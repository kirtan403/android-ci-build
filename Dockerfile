FROM openjdk:8-jdk
MAINTAINER Kitan Thakkar <kirtan403@gmail.com>

ARG ANDROID_BUILD_TOOLS
ARG ANDROID_PLATFORM
ENV ANDROID_BUILD_TOOLS ${ANDROID_BUILD_TOOLS}
ENV ANDROID_PLATFORM ${ANDROID_PLATFORM}

RUN apt-get --quiet update --yes
RUN apt-get --quiet install --yes wget tar unzip lib32stdc++6 lib32z1

RUN wget --quiet --output-document=tools.zip https://dl.google.com/android/repository/sdk-tools-linux-3859397.zip && \
    unzip tools.zip -d ./android-sdk-linux

ENV ANDROID_HOME $PWD/android-sdk-linux
ENV PATH $PATH:$PWD/android-sdk-linux/platform-tools/
    
RUN mkdir -p /root/.android && \
    touch ~/.android/repositories.cfg

RUN echo "y" | $ANDROID_HOME/tools/bin/sdkmanager --licenses && \
    echo "y" | $ANDROID_HOME/tools/bin/sdkmanager "build-tools;${ANDROID_BUILD_TOOLS}" && \
    echo "y" | $ANDROID_HOME/tools/bin/sdkmanager "platforms;android-${ANDROID_PLATFORM}" && \
    echo "y" | $ANDROID_HOME/tools/bin/sdkmanager "tools" "platform-tools" && \
    echo "y" | $ANDROID_HOME/tools/bin/sdkmanager "extras;android;m2repository" "extras;google;m2repository"